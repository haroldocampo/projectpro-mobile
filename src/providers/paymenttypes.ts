import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { User } from './user';
import { Api } from './api';

import { PaymentType } from '../models/paymenttype';

@Injectable()
export class PaymentTypes {
  userEmail: string;

  constructor(public http: Http, public api: Api, user: User) {
    this.userEmail = user._user;
  }

  query(employeeId: any, companyId: any) {
    // return this.api.get('api/paymentTypes', params)
    //   .map(resp => resp.json());
    // /api/company/{id}/user/{employeeid}/paymentTypes
    // return this.api.get('api/company/' + companyId + '/user/' + employeeId + '/paymentTypes')
    //   .map(resp => resp.json());
    var isSuperAdmin = false;
    if (!employeeId) {
      isSuperAdmin = true;
    }
    return this.api.get('api/paymentTypes?companyId=' + companyId + '&employeeId=' + employeeId + '&isSuperAdmin=' + isSuperAdmin)
      .map(resp => resp.json());
  }

  add(paymenttype: PaymentType) {
  }

  delete(paymenttype: PaymentType) {
  }
}
