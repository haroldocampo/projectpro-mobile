import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from './user';
import { Api } from './api';

import { Project } from '../models/project';

@Injectable()
export class Projects {

  constructor(public http: Http, public api: Api, user: User) {
  }

  query(id: any) {
    return this.api.get('api/company/' + id + '/projects')
      .map(resp => resp.json());
  }

  add(project: Project) {
  }

  delete(project: Project) {
  }
}
