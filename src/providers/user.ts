import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Api } from './api';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Storage } from '@ionic/storage';

/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;
  _companyId: any;
  _employeeId: any;
  _id: any;
  errorMessage: string = '';
  status: string = '';

  constructor(public http: Http, public api: Api, public storage: Storage) {
  }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    this.errorMessage = "";
    // var payload: string = "";
    let payload = "email=" + encodeURIComponent(accountInfo.email) + "&password=" + encodeURIComponent(accountInfo.password);
    // let payload = {
    //   'email': accountInfo.email,
    //   'password': accountInfo.password
    // };
    // console.log("payload: " + payload);

    // let urlSearchParams = new URLSearchParams();
    // urlSearchParams.append("email", encodeURIComponent(accountInfo.email));
    // urlSearchParams.append("password", encodeURIComponent(accountInfo.password));
    // let payload = urlSearchParams.toString

    console.log(payload);

    // return this.api.post('mobile/verify', payload);
    return this.api.post('mobile/verify?XDEBUG_SESSION_START=PHPSTORM', payload);
    // let seq = this.api.post('mobile/verify?XDEBUG_SESSION_START=PHPSTORM', payload);

    // seq
    //   .map(res => res.json())
    // .subscribe(res => {
    //     // If the API returned a successful response, mark the user as logged in
    //     this.status = res.status.toLowerCase();
    //     if (this.status == 'success') {
    //       this.storage.set('companyId', res.companyId);
    //       this.storage.set('userId', res.userId);
    //       this.storage.set('employeeId', res.employeeId);
    //       this._loggedIn(accountInfo.email, res.companyId);
    //       this._id = res.userId;
    //       this._employeeId = res.employeeId;
    //     } else {
    //       this.errorMessage = 'Username/Password is incorrect.';
    //     }
    //
    //   }, err => {
    //     console.error('ERROR[login]', err);
    //   });
    //
    // return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    return this.api.post('mobile/adduser', accountInfo);
  }

  /**
 * Send a POST request to our resetpw endpoint with the email address
 * the user entered on the form.
 */
  reset(accountInfo: any) {
    console.log("reset before paylod: "+ accountInfo);
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append("username", encodeURIComponent(accountInfo));
    urlSearchParams.append("m", encodeURIComponent("yes"));
    let body = urlSearchParams.toString();
    // let payload = "username=" + accountInfo + "&m=yes";
    // console.log("payload: " + payload)
    // return this.api.post('resetting/send-email', payload);
    return this.api.post('resetting/send-email?XDEBUG_SESSION_START=PHPSTORM', body);
  }
  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this.storage.set('firsttime', false);
    this.storage.remove('email');
    this.storage.remove('pwd');
    this.storage.remove('companyId');
    this.storage.remove('userId');
    this.storage.remove('employeeId');
    this.storage.remove('savedCompany')
    this.storage.remove('isSuperAdmin');

    this.storage.remove('companies');
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(email, companyId?) {
    this._user = email;
    if (companyId) {
      this._companyId = companyId;
    }
  }
}
