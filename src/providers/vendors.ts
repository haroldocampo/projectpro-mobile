import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from './user';
import { Api } from './api';

import { Vendor } from '../models/vendor';

@Injectable()
export class Vendors {

  constructor(public http: Http, public api: Api, user: User) {
  }

  query(id: any) {
    return this.api.get('api/company/' + id + '/vendors')
      .map(resp => resp.json());
  }

  add(id: any, vendor: any) {      
    return this.api.post('api/company/' + id + '/vendors',vendor)
    .map(resp => resp.json());
  }
}
