import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from './user';
import { Api } from './api';

import { Purchase } from '../models/purchase';
import { Storage } from '@ionic/storage';

@Injectable()
export class Purchases {
  constructor(public http: Http, public api: Api, user: User, public storage: Storage) {

  }

  query(id: any, isSuperAdmin: any, companyId: any) {
    // let params = {
    //   userId: id,
    //   //cmd: 'list'
    // };
    return this.api.get('mobile/purchases?userId=' + id + '&isSuperAdmin=' + isSuperAdmin + '&companyId=' + companyId)
      .map(resp => resp.json());
  }

  getCompanies(employeeId: any) {
    return this.api.get('api/companies?employeeId=' + employeeId)
      .map(resp => resp.json());
  }

  getPurchaseImage(id?: any) {
    let params = 'data=' + id;
    return this.api.post('getpurchaseimage/', params)
      .map(resp => resp.json());
  }

  submit(purchase: any) {
    let seq = this.api.post('submitpurchase/', purchase);

    seq
      .map(res => res.json())
      .subscribe(res => {
        if (res.status == 'success') {

        } else {
        }
      }, err => {
        console.error('ERROR[submit]', err);
      });

    return seq;
  }

  delete(purchase: Purchase) {
  }

}
