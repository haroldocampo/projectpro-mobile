import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, PopoverController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

import { Vendors } from '../../providers/providers';
import { Vendor } from '../../models/vendor';
import { Purchase } from '../../models/purchase';
import { MainPage } from '../../pages/pages';
import { Camera } from '@ionic-native/camera';
import { AlertController } from 'ionic-angular';
import { PaymentTypeListPage } from '../payment-type-list/payment-type-list';
import { ImagePopoverPage } from '../popups/popimage';
@Component({
  selector: 'vendor',
  templateUrl: 'vendor.html'
})
export class VendorPage {
  vlist: Vendor[];
  oList: Observable<any>;
  receipt: any;
  isNewProject: boolean = false;
  form: FormGroup;
  isReadyToSave: boolean = false;
  fromEdit: boolean = false;
  employeeId: any;
  companyId: any;
  dataLoadDone: boolean = false;
  tempList: any;
  isSearching: boolean = false;
  selectedVendor: any;
  hasSelected: boolean = false;
  finalVendor: any;
  filteredList : Vendor[];
  purchase: Purchase = new Purchase({});
  ts = { progress: null, result: null, error: null, resultorerror: null };
  constructor(public viewCtrl: ViewController, public plt: Platform, public storage: Storage, public popoverCtrl: PopoverController, public toastCtrl: ToastController,
    public navCtrl: NavController,
    public vendors: Vendors,
    public alertCtrl: AlertController,
    formBuilder: FormBuilder,
    navParams: NavParams,
    public camera: Camera) {

    this.fromEdit = navParams.get('fromEdit');    
    this.purchase.imageData = navParams.get('newPurchasePhoto');

    storage.get('employeeId').then((val) => {
      this.employeeId = val;
    });

    storage.get('companyId').then((val) => {
      this.companyId = val;
      // check if has
      console.log("companyID: " + val);
      if (val != 0 && val) {
        this.vlist = [];
        this.oList = this.vendors.query(val);
        this.oList.subscribe(res => {          
          //  this.vlist = res.vendors;   
           if (res.vendors.length > 0) {
            for (let _v in res.vendors) {
              this.vlist.push(new Vendor(res.vendors[_v]));
            }
          }
        }, err => {
          let toast = this.toastCtrl.create({
            message: "Unable to fetch the vendors.",
            duration: 3000,
            position: 'top'
          });
          toast.present();
        }, () => console.log('dne'));
      } else {
        this.isNewProject = true;
      }
    });

    //    this.plist = this.projects.query();
    this.form = formBuilder.group({
      name: ['', Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

    // this.showtip();
  }

  home() {
    this.navCtrl.setRoot(MainPage);
  }

  search(selectedVendor) {
    
    if (selectedVendor != null) {      
      this.isSearching = true;
      this.filteredList = this.vlist;
      this.filteredList = this.vlist.filter(v => v['name'].toLowerCase().indexOf(selectedVendor.toLowerCase())>= 0);
      this.filteredList.sort(function(a, b){        
        if(a['name'] < b['name']) return -1;
        if(a['name'] > b['name']) return 1;
          return 0;
      })
    }else {
      this.isSearching = true;
      this.filteredList = this.vlist;
      this.filteredList.sort(function(a, b){
        if(a['name'] < b['name']) return -1;
        if(a['name'] > b['name']) return 1;
          return 0;
      })
    }
  }

  select(vendor) {        
    if (vendor instanceof Vendor) {      
      this.selectedVendor = vendor['name'];
    } else {      
      this.selectedVendor = vendor;
    }

    this.isSearching = false;
    this.hasSelected = true;
  }

  clear() {
    this.isSearching = false;
    this.hasSelected = false;
    this.selectedVendor = null;
  }

  done() {
    let observer : Observable<any>;
    let sendParams : any;
    sendParams = {
      name : this.selectedVendor,
      purchaser: this.employeeId
    }
    observer = this.vendors.add(this.companyId,sendParams);
    observer.subscribe(res=>{
      console.log(res.vendor);
      this.navCtrl.push(PaymentTypeListPage, { fromEdit: false, vendor: res.vendor, purchase: this.purchase });
      
    }, err=>{
      console.log(err);
    });  
  }
  showImage() {
    console.log('Show Image :)');
    let popover = this.popoverCtrl.create(ImagePopoverPage, { purchase: this.purchase }, { cssClass: 'image-popover' });
    popover.present();
  }
  
}
