import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController } from 'ionic-angular';

import { FirstRunPage } from '../../pages/pages';
import { User } from '../../providers/user';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  confirmPassword: string;
  account: {
    firstname: string,
    lastname: string,
    isAccountant: boolean,
    accountantEmail: string,
    email: string, password: string
  } = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    isAccountant: false,
    accountantEmail: ''
  };

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

  }
  isConfirmed(): boolean {
    return this.account.password == this.confirmPassword;
  }

  isValidEmail(): boolean {
    let emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (emailPattern.test(this.account.email)) {
      return true;
    } else {
      return false;
    }
  }

  changeCase(event) {
    this.account[event.target.name] = this.account[event.target.name].charAt(0).toUpperCase() + this.account[event.target.name].toLowerCase().slice(1);
  }
  doSignup() {
    if (this.account.email == '' || this.account.password == '' || this.account.firstname == '' || this.account.lastname == '') {
      let toast = this.toastCtrl.create({
        message: "Please fill up the fields to continue.",
        duration: 3000,
        position: 'top'
      });
      toast.present();
      return;
    }

    if (!this.isValidEmail()) {
      let alert = this.alertCtrl.create({
        title: 'Invalid Email',
        message: 'Type a valid email.',
        buttons: ['OK']
      });
      alert.present();
      return
    }

    if (this.account.password != this.confirmPassword) {
      let alert = this.alertCtrl.create({
        title: 'Password Error',
        message: 'Password does not match.',
        buttons: ['OK']
      });
      alert.present();
      return
    }

    if (this.account.password.length < 6) {
      let alert = this.alertCtrl.create({
        title: 'Password Error',
        message: 'Minimum password length is 6 characters.',
        buttons: ['OK']
      });
      alert.present();
      return
    }

    let loader = this.loadingCtrl.create();

    loader.present();
    // Attempt to signup in through our User service
    var payload: string = "";
    payload = "data=" + this.account.email + "," + this.account.password + "," + this.account.firstname + "," + this.account.lastname + "," + (this.account.accountantEmail == "" ? 'none' : this.account.accountantEmail) + "," + (this.account.isAccountant ? 'accountant' : 'field-emp');
    this.user.signup(this.account).subscribe((resp) => {
      let alert = this.alertCtrl.create({
        title: 'Sign Up Successful',
        message: 'Sign in to start using ProjectPro',
        buttons: ['OK']
      });
      loader.dismissAll();
      alert.present();
      this.navCtrl.push(FirstRunPage);
    }, (err) => {
      // Unable to sign up
      loader.dismissAll();
      let toast = this.toastCtrl.create({
        message: "Unable to complete the signup." + err.statusText,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
