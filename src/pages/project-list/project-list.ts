import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, PopoverController, ToastController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

import { Projects } from '../../providers/providers';
import { Project } from '../../models/project';
import { Vendor } from '../../models/vendor';
import { CostCodeListPage } from '../cost-code-list/cost-code-list';
import { Purchase } from '../../models/purchase';
import { MainPage } from '../../pages/pages';
import { Camera } from '@ionic-native/camera';
import { Tooltips } from '../popups/tooltips';
import { AlertController } from 'ionic-angular';
import { ImagePopoverPage } from '../popups/popimage';
import { PaymentType } from '../../models/paymenttype';

@Component({
  selector: 'page-project-list',
  templateUrl: 'project-list.html'
})
export class ProjectListPage {
  OCRAD: any;
  plist: Project[];
  oList: Observable<any>;
  sort = { "projectId": "fa-unsorted", "projectName": "fa-unsorted" };
  receipt: any;
  isNewProject: boolean = false;
  purchase: Purchase;
  paymentType: PaymentType;
  form: FormGroup;
  isReadyToSave: boolean = false;
  fromEdit: boolean = false;
  employeeId: any;
  dataLoadDone: boolean = false;
  vendor: Vendor;

  ts = { progress: null, result: null, error: null, resultorerror: null };
  constructor(public viewCtrl: ViewController, public plt: Platform, public storage: Storage, public popoverCtrl: PopoverController, public toastCtrl: ToastController,
    public navCtrl: NavController,
    public projects: Projects,
    public alertCtrl: AlertController,
    formBuilder: FormBuilder,
    navParams: NavParams,
    public camera: Camera) {

    this.fromEdit = navParams.get('fromEdit');
    // this.purchase.imageData = navParams.get('newPurchasePhoto');
    this.purchase = navParams.get('purchase');
    this.paymentType = navParams.get('paymentType');
    this.vendor = navParams.get('vendor');
    storage.get('employeeId').then((val) => {
      this.employeeId = val;
    });

    storage.get('companyId').then((val) => {
      // check if has
      console.log("companyID: " + val);      
      if (val != 0 && val) {
        this.plist = [];
        this.oList = this.projects.query(val);
        this.oList.subscribe(res => {
          // If the API returned a successful response, retrive all projects returned
          if (res.status.toLowerCase() == 'success') {
            if (res['projects'].length > 0) {
              for (let _p in res['projects']) {
                this.plist.push(new Project(res['projects'][_p]));
              }
            }

            this.plist.push(new Project({
              "id": 0,
              "name": "Project not found"
            }));
            console.log(this.plist);
            this.dataLoadDone = true;
          } else {
            const alert = this.alertCtrl.create({
              title: 'Server Error',
              message: 'An error occured',
              buttons: ['Dismiss']
            });
            alert.present();
          }
        }, err => {
          let toast = this.toastCtrl.create({
            message: "Unable to fetch the projects.",
            duration: 3000,
            position: 'top'
          });
          toast.present();
        }, () => console.log('dne'));
      } else {
        this.isNewProject = true;
      }
    });

    //    this.plist = this.projects.query();
    this.form = formBuilder.group({
      name: ['', Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });

    // this.showtip();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    //    this.viewCtrl.setBackButtonText('Purchases');
  }
  ionViewWillEnter() {
    if (!this.fromEdit && this.purchase.imageData == null) {
      // this.getPicture();
    }
  }

  showtip() {
    this.storage.get('loadprojects').then((val) => {
      if (val == null || val) {
        let popover = this.popoverCtrl.create(Tooltips, { page: 'project-list', top: 100 }, { showBackdrop: true });
        popover.present();
      }
      this.storage.set('loadprojects', false);
    });
  }
  home() {
    this.navCtrl.setRoot(MainPage);
  }

  doSort(col) {
    var order = "";
    var current = this.sort[col];
    var iOrder = 1;
    switch (current) {
      case "fa-unsorted":
      case "fa-sort-desc": order = "fa-sort-asc"; break;
      case "fa-sort-asc": order = "fa-sort-desc"; iOrder = -1;
    }
    this.sort[col] = order;
    if (col == 'projectId') {
      this.sort['projectName'] = 'fa-unsorted';
    } else {
      this.sort['projectId'] = 'fa-unsorted';
    }
    this.plist.sort(function(a, b) {
      if (a[col] < b[col]) {
        return -1 * iOrder;
      }
      if (a[col] > b[col]) {
        return 1 * iOrder;
      }

      return 0;
    });
  }
  /**
   * Select project
   */
  select(project) {
    if (project.id != 0) {
      // this.navCtrl.push(PaymentTypeListPage, { project: project, purchase: this.purchase });
      this.navCtrl.push(CostCodeListPage, { project: project, paymentType: this.paymentType, purchase: this.purchase, vendor: this.vendor });
    } else {
      this.noProject();
    }
    console.log(project);
  }

  noProject() {
    if (this.employeeId) {
      let alert = this.alertCtrl.create({
        title: 'Project not found',
        message: 'Please contact an Administrator in your company to add a new project.',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.isNewProject = true;
    }
  }

  newProject() {
    var np: Project = new Project({
      "id": 1,
      "name": this.form.controls['name'].value
    });
    this.select(np);
  }

  showImage() {
    console.log('Show Image :)');
    let popover = this.popoverCtrl.create(ImagePopoverPage, { purchase: this.purchase }, { cssClass: 'image-popover' });
    popover.present();
  }

}
