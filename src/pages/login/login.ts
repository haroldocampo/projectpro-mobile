import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MainPage } from '../../pages/pages';

import { User } from '../../providers/user';
import { SignupPage } from '../signup/signup';
import { ResetpwPage } from '../resetpw/resetpw';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };
  remember: any;
  constructor(public navCtrl: NavController,
    public user: User, public storage: Storage,
    public toastCtrl: ToastController, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
    this.remember = false;
    storage.get('email').then((val) => {
      console.log(val);
      this.account.email = val;
      this.remember = true;

      if (this.account.email) {
        this.navCtrl.setRoot(MainPage);
      }
    });
    storage.get('pwd').then((val) => {
      this.account.password = val;
    });
  }

  skipLogin() {
    this.navCtrl.setRoot(MainPage);
  }

  // Attempt to login in through our User service
  doLogin() {
    let loader = this.loadingCtrl.create({
      content: 'Logging in...'
    });
    loader.present();
    console.log(this.account.email);

    this.user.login(this.account).map(resp => resp.json()).subscribe((resp) => {
      let status = resp.status;
      console.log(resp);
      console.log("login.ts, user.status: " + status);

      if (status == 'success') {
        this.storage.set('userId', resp.userId);
        this.storage.set('isSuperAdmin', resp.isSuperAdmin);
        this.storage.set('companies', JSON.stringify(resp.companies))
        this.storage.remove('local_name');
        this.storage.remove('local_accountantEmail');
        if (this.remember) {
          this.storage.set('email', this.account.email);
          this.storage.set('pwd', this.account.password);
        } else {
          this.storage.remove('email');
          this.storage.remove('pwd');
        }
        this.navCtrl.setRoot(MainPage);
      } else if (status == 'disabled') {
        let alert = this.alertCtrl.create({
          title: 'Account Disabled',
          message: 'Please contact your administrator.',
          buttons: ['OK']
        });
        alert.present();
      } else {
        // let toast = this.toastCtrl.create({
        //   message: resp.errorMessage,
        //   duration: 3000,
        //   position: 'top'
        // });
        // toast.present();
        let alert = this.alertCtrl.create({
          title: 'Invalid Credentials',
          message: 'Ooops. This account does not exist. Please try a different username and/or password.',
          buttons: ['OK']
        });
        alert.present();
      }
      loader.dismissAll();
    }, (err) => {
      //      this.navCtrl.setRoot(MainPage);
      // Unable to log in
      loader.dismissAll();
      let toast = this.toastCtrl.create({
        message: 'Unable to login.\n' + err.statusText,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
  signup() {
    this.navCtrl.push(SignupPage);
  }
  resetpw() {
    this.navCtrl.push(ResetpwPage);
  }
}
