/**
 * Add amounts page
 * - lists all cost codes selected
 * - append salestax to all purchases
 */
import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, ViewController, NavParams, LoadingController, PopoverController, AlertController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

import { PaymentType } from '../../models/paymenttype';
import { Purchase } from '../../models/purchase';
import { Project } from '../../models/project';
import { Vendor } from '../../models/vendor';
import { CostCode } from '../../models/costcode';

import { PurchaseReviewPage } from '../purchase-review/purchase-review';
import { MainPage } from '../../pages/pages';
import { CalcPopup } from '../popups/calc';
import { ImagePopoverPage } from '../popups/popimage';

import * as moment from 'moment';

@Component({
  selector: 'page-add-amounts',
  templateUrl: 'add-amounts.html'
})
export class AddAmountsPage {
  project: Project;
  purchase: Purchase;
  vendor: Vendor;
  isNewCostCode: boolean = false;
  form: FormGroup;
  isReadyToSave: boolean = false;
  paymentType: PaymentType;
  costcodes: Array<CostCode>;
  balance: number;
  origBalance = 0;
  hasBalance: boolean = false;
  strDop: string;
  dop: any;

  constructor(public loadingCtrl: LoadingController, public viewCtrl: ViewController, public navCtrl: NavController, formBuilder: FormBuilder, navParams: NavParams,
    public popoverCtrl: PopoverController, public alertCtrl: AlertController, private datePicker: DatePicker) {
    this.purchase = navParams.get('purchase');
    this.project = navParams.get('project');
    this.vendor = navParams.get('vendor');
    this.paymentType = navParams.get('paymentType');
    this.costcodes = navParams.get('costcodes');

    var controls = {};
    this.costcodes.forEach(costcode => {
      controls["cc_" + costcode["costCodeId"]] = ['', Validators.required];
    });
    controls['balance'] = ['', Validators.required];
    controls['salestax'] = ['', Validators.required];
    controls['totalpa'] = ['', Validators.required];
    this.form = formBuilder.group(controls);

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      //      this.isReadyToSave = this.form.valid;
    });

    this.dop = moment();
    this.strDop = this.dop.format('MM.DD.YY');
  }

  /* View load event */
  ionViewDidLoad() {
    //    this.viewCtrl.setBackButtonText('Cost Codes');
  }

  /* Update the balance whenever amounts are inputted to the cost codes*/
  updateBalance() {

    var subtotal = 0;
    var self = this;
    this.costcodes.forEach(cc => {
      subtotal += parseFloat((self.form.controls['cc_' + cc['costCodeId']].value == "" ? 0 : self.form.controls['cc_' + cc['costCodeId']].value));
    });
    subtotal += parseFloat((self.form.controls['salestax'].value == "" ? 0 : self.form.controls['salestax'].value));
    if (this.hasBalance) {
      this.balance = this.origBalance - subtotal;
      this.isReadyToSave = (this.balance == 0);
    } else {
      this.origBalance = subtotal;
      this.isReadyToSave = true;
    }
  }

  home() {
    this.navCtrl.setRoot(MainPage);
  }
  resetAmounts() {
    this.origBalance = this.balance;
    this.costcodes.forEach(cc => {
      this.form.controls['cc_' + cc['costCodeId']].setValue(0);
    });
  }

  /* Executed when the submit is tapped */
  review() {
    var costs = [];
    var missing = false;
    this.costcodes.forEach(cc => {
      let value = this.form.controls['cc_' + cc['costCodeId']].value;

      if (!value) {
        missing = true;
      }

      costs.push({
        costId: cc['costCodeId'],
        cost: { 'description': cc['costCodeName'], 'code_number': cc['costCodeNumber'] },
        amount: this.form.controls['cc_' + cc['costCodeId']].value
      });
    });
    this.purchase['amount'] = this.origBalance;
    let st = this.form.controls['salestax'].value;
    let tp = this.form.controls['totalpa'].value;

    if (!tp || missing) {
      this.presentMissing();
      return;
    }

    // if (tp <= 0) {
    //   let alert = this.alertCtrl.create({
    //     title: 'Invalid Value',
    //     message: 'Total Purchase Amount cannot be less than or equal to zero.',
    //     buttons: ['OK']
    //   });
    //   alert.present();
    //   return;
    // }

    if (!st) {
      st = 0;
    }

    // if (st <= 0) {
    //   let alert = this.alertCtrl.create({
    //     title: 'Invalid Value',
    //     message: 'Sales Tax cannot be less than or equal to zero.',
    //     buttons: ['OK']
    //   });
    //   alert.present();
    //   return;
    // }

    console.log(this.dop.format('MM/DD/YYYY'));
    if (this.balance == 0 || !this.hasBalance) {
      this.navCtrl.push(PurchaseReviewPage, {
        stax: st,
        costs: costs,
        purchase: this.purchase,
        project: this.project,
        vendor: this.vendor,
        paymentType: this.paymentType,
        viewOnly: false,
        totalpa: tp,
        dop: this.dop.format('MM/DD/YYYY')
      });
    } else {
      alert('Amounts does not match. Please check.');
    }
  }

  /* Reads the receipt and tries to extract the total amount */
  analyze() {
    let loader = this.loadingCtrl.create({
      content: 'I\'m trying to read your receipt. Please wait...'
    });
    loader.present();
    /* Use OCRAD function to read the image loaded at the hidden img tag */
    (<any>window).OCRAD(document.getElementById('image'), text => {
      loader.dismissAll();
      console.log(text);
      var posTotal = text.toLowerCase().indexOf('total');
      if (posTotal > -1) {
        this.hasBalance = true;
        this.balance = parseFloat(text.substr(posTotal));
      } else {
        this.hasBalance = false;
      }
    });
  }


  showCalc(event) {
    let popover = this.popoverCtrl.create(CalcPopup, { target: this.form.controls[event.target.attributes['ng-reflect-name'].value] }, { enableBackdropDismiss: false, cssClass: 'custom-popover' });
    popover.present();
    popover.onDidDismiss(ev => { this.updateBalance(); })
  }

  presentMissing() {
    let alert = this.alertCtrl.create({
      title: 'Missing Fields',
      message: 'Please populate all fields.',
      buttons: ['OK']
    });
    alert.present();
  }

  showImage() {
    console.log('Show Image :)');
    let popover = this.popoverCtrl.create(ImagePopoverPage, { purchase: this.purchase }, { cssClass: 'image-popover' });
    popover.present();
  }

  selectDate() {
    console.log("date clicked");
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.dop = moment(date);
        this.strDop = this.dop.format('MM.DD.YY');
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

}
