import { Component, ViewChild } from '@angular/core';
import * as moment from 'moment';
import {
  NavController, NavParams, ModalController, ViewController,
  PopoverController, ToastController, AlertController, Platform, LoadingController, ActionSheetController
} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
// import * as Tesseract from 'tesseract.js';

import { ProjectListPage } from '../project-list/project-list';
import { VendorPage } from '../vendor/vendor';
import { PurchaseReviewPage } from '../purchase-review/purchase-review';
import { Purchases } from '../../providers/providers';
import { Purchase } from '../../models/purchase';
import { User } from '../../providers/user';
import { LoginPage } from '../login/login';
import { Tooltips } from '../popups/tooltips';
import { Camera } from '@ionic-native/camera';
import { Api } from '../../providers/api';

@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html',
  queries: {
    addReceipt: new ViewChild('addReceipt')
  }
})
export class ListMasterPage {
  plist: Purchase[];
  // cList: Company[];
  companies = [];
  selectedCompany = "";
  showCompanyPicker: boolean = false;

  url: string = this.api.url + '/api/companies';

  oList: Observable<any>;
  cList: Observable<any>;
  sort = { "date": "fa-unsorted", "vendor": "fa-unsorted" };

  constructor(public popoverCtrl: PopoverController, public viewCtrl: ViewController, public storage: Storage, public navParams: NavParams, public toastCtrl: ToastController,
    public navCtrl: NavController, public purchases: Purchases, public modalCtrl: ModalController, private alertCtrl: AlertController, public user: User, public camera: Camera,
    public plt: Platform, public loadingCtrl: LoadingController, public api: Api, private actionSheetCtrl: ActionSheetController) {

    // check user
    this.loadData();
  }

  loadData(refresher = null) {
    this.storage.get('companies').then((val) => {
      console.log('get companies: ' + val);
      if (!val) {
        console.log("starting local mode");
        // this.user.logout();
        // this.logout();
        this.checkLocal();
      } else {
        let companies = JSON.parse(val);
        this.companies = companies;
        let companiesCount = companies.length;
        console.log("list-master, companies: " + companiesCount);
        console.log(companies);

        if (companiesCount > 1) {
          this.showCompanyPicker = true;
        }

        if (companiesCount > 0) {
          this.storage.get('savedCompany').then((val) => {
            var company = companies[0];
            if (val) {
              company = val;
            }

            this.loadCompanyPurchases(company);
            if (refresher) {
              refresher.complete();
            }
          });
        }
      }
    });
  }

  doRefresh(refresher) {
    this.loadData(refresher);
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    if (this.companies.length > 0) {
      this.loadCompanies(this.companies[0]);
    }
  }

  checkLocal() {
    this.storage.get('local_name').then((val) => {
      if (!val) {
        // request for user
        let alert = this.alertCtrl.create({
          title: 'Welcome!',
          message: 'Please input your name. It will be shown on the email that we will be sending to your accountant.',
          inputs: [
            {
              name: 'name',
              placeholder: 'Enter your name here'
            }
          ],
          buttons: [
            {
              text: 'Save',
              handler: data => {
                let name = data.name;
                if (name.trim().length === 0) {
                  console.log('name cannot be empty');
                } else {
                  this.storage.set('local_name', name);
                  this.showWelcomeMessage(name);
                }
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.present();
      } else {
        this.showWelcomeMessage(val);
      }
    });
  }

  showWelcomeMessage(name) {
    // let toast = this.toastCtrl.create({
    //   message: 'Welcome ' + name + '!',
    //   duration: 3000,
    //   position: 'bottom'
    // });
    // toast.present();
  }

  loadCompanies(company) {
    let employeeId = company["employeeId"];
    this.cList = this.purchases.getCompanies(employeeId);
    this.cList.subscribe(res => {
      // If the API returned a successful response, mark the user as logged in
      let companies = res;
      this.companies = companies;
      this.storage.set('companies', JSON.stringify(companies))
    }, err => {
      console.log('Cannot load companies');
      // let toast = this.toastCtrl.create({
      //   message: "Unable to connect to network",
      //   duration: 3000,
      //   position: 'top'
      // });
      // toast.present();
    }, () => console.log('Refreshed companies')
    );
  }

  loadCompanyPurchases(company) {

    company.color = 'danger';
    for (let c of this.companies) {
      if (c == company) {
        c.color = 'primary';
      } else {
        c.color = 'none';
      }
    }

    this.selectedCompany = company["name"];
    this.storage.get('isSuperAdmin').then((isSuperAdmin) => {
      console.log('isSuperAdmin: ' + isSuperAdmin);

      let employeeId = company["employeeId"];
      let companyId = company["companyId"];
      this.storage.set('companyId', companyId);
      this.storage.set('savedCompany', company);
      this.storage.set('employeeId', employeeId);

      console.log("showing purchases for employee # " + employeeId);

      this.plist = [];
      this.oList = this.purchases.query(employeeId, isSuperAdmin, companyId);
      this.oList.subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.status.toLowerCase() == 'success') {
          if (res['purchases'].length > 0) {
            for (let _p in res.purchases) {
              //var jsonData = res['body'][_p]['purchase_data'];
              let date = res['purchases'][_p].date.substring(0, 10);
              // console.log('api: ' + date);
              // console.log('moment: ' + moment(date).format());
              this.plist.push(new Purchase({
                vendor: '',
                imageData: res['purchases'][_p].imageUrl,//'data:image/jpeg;base64,' + res['body'][_p]['imageData'],
                date: date,
                amount: res['purchases'][_p].amount,
                totalPurchaseAmount: res['purchases'][_p].totalPurchaseAmount,
                purchase_id: res['purchases'][_p].id,
                purchaseData: res['purchases'][_p].data
              }));
            }
          }
        }
      }, err => {
        console.log('cannot load purchases');
        // let toast = this.toastCtrl.create({
        //   message: "Unable to connect to network",
        //   duration: 3000,
        //   position: 'top'
        // });
        // toast.present();
      }, () => console.log('Loaded purchases')
      );
    });


  }

  showtip() {
    this.storage.get('loadmaster').then((val) => {
      if (val == null || val) {
        let popover = this.popoverCtrl.create(Tooltips, { page: 'list-master', top: 100 }, { showBackdrop: true });
        popover.present();
      }
      this.storage.set('loadmaster', false);
    });
  }

  /**
   * This will sort the main list according to the column that was tapped.
   */
  doSort(col) {
    var order = "";
    var iOrder = 1;
    var current = this.sort[col];
    switch (current) {
      case "fa-unsorted":
      case "fa-sort-desc": order = "fa-sort-asc"; break;
      case "fa-sort-asc": order = "fa-sort-desc"; iOrder = -1;
    }
    this.sort[col] = order;
    if (col == 'date') {
      this.sort['vendor'] = 'fa-unsorted';
    } else {
      this.sort['date'] = 'fa-unsorted';
    }
    this.plist.sort(function(a, b) {
      if (a[col] < b[col]) {
        return -1 * iOrder;
      }
      if (a[col] > b[col]) {
        return 1 * iOrder;
      }

      return 0;
    });
  }
  /**
   * This will load the list of projects page
   */
  addPurchase() {        
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Modify your album',
      buttons: [
        {
          text: 'Choose from library',
          handler: () => {
            console.log('Archive clicked');
            this.getPicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Take a photo',
          handler: () => {
            console.log('Destructive clicked');
            this.getPicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  /**
   * Delete an un-approved purchase from the list of items.
   */
  deleteItem(item) {
    this.purchases.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  viewPurchase(purchase: Purchase) {
    var purchase_id = purchase['purchase_id'];
    var totalPurchaseAmount = purchase['totalPurchaseAmount'];
    var stax = purchase['purchaseData']['sales_tax'];//purchase['data']['salestax'];
    var costs = purchase['purchaseData']['purchase_items'];
    var project = purchase['purchaseData']['project'];
    var paymentType = purchase['purchaseData']['payment_type'];
    var vendor = purchase['purchaseData']['vendor'];
    this.navCtrl.push(PurchaseReviewPage, {
      stax: stax, costs: costs, purchase: purchase, project: project, paymentType: paymentType, purchase_id: purchase_id, totalpa: totalPurchaseAmount, vendor: vendor,
      viewOnly: true
    });
  }

  /**
   * Logout
   */
  logout() {
    this.navCtrl.setRoot(LoginPage);
  }

  confirmLogout() {
    this.storage.get('local_name').then((val) => {
      if (!val) {
        let alert = this.alertCtrl.create({
          title: 'Confirm logout',
          message: 'Do you want to continue?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Logout',
              handler: () => {
                console.log('Buy clicked');
                this.user.logout();
                this.logout();
              }
            }
          ]
        });
        alert.present();
      } else {
        this.user.logout();
        this.logout();
      }
    });
  }

  getPicture(sourceType) {
    if (Camera['installed']()) {
      let loader = this.loadingCtrl.create({

      });
      loader.present();

      var dest: any;
      if (this.plt.is('android')) {
        dest = this.camera.DestinationType.FILE_URI
      } else if (this.plt.is('ios')) {
        dest = this.camera.DestinationType.NATIVE_URI
      } else {
        dest = this.camera.DestinationType.DATA_URL
      }
      this.camera.getPicture({
        //destinationType: dest,
        destinationType: this.camera.DestinationType.FILE_URI,
        //destinationType: this.camera.DestinationType.DATA_URL, // Harold Ocampo change
        sourceType: sourceType,
        quality: 40,
        cameraDirection: 0, //back cameraxw
        // allowEdit: true,
        saveToPhotoAlbum: false,
        correctOrientation: true
      }).then((imageData) => {
        loader.setContent('Processing image...');
        // this.navCtrl.push(ProjectListPage, { fromEdit: false, newPurchasePhoto: imageData });
        this.navCtrl.push(VendorPage, { fromEdit: false, newPurchasePhoto:imageData });
        // Tesseract.recognize(imageData)
        //   .progress(message => console.log('progress:', message))
        //   .catch(err => console.log('err:', err))
        //   .then(result => console.log('result:' + result['text']))
        //   .finally(resultOrError => console.log('resultOrError:', resultOrError));
        loader.dismissAll();
      }, (err) => {
        console.log('Cancelled receipt');
        let alert = this.alertCtrl.create({
          title: 'Photo not taken',
          message: 'Take a photo of the receipt to continue.',
          buttons: ['OK']
        });
        loader.dismissAll();

        alert.present();
        this.navCtrl.pop();

        // alert('Unable to take photo');

        // temporary bypass for ionic serve
        // this.navCtrl.push(ProjectListPage, { fromEdit: false });
      })

    } else {
      //alert('Camera access is needed.');
      //this.navCtrl.pop();

      // temporary bypass for ionic serve
      // this.navCtrl.push(ProjectListPage, { fromEdit: false });
      this.navCtrl.push(VendorPage, { fromEdit: false });
    }
  }
}
