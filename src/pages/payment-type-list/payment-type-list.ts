import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, PopoverController, ToastController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';

import { PaymentTypes } from '../../providers/providers';
import { PaymentType } from '../../models/paymenttype';
import { Vendor } from '../../models/vendor';
import { Purchase } from '../../models/purchase';
import { ProjectListPage } from '../project-list/project-list';
import { Tooltips } from '../popups/tooltips';

import { MainPage } from '../../pages/pages';

@Component({
  selector: 'page-payment-type-list',
  templateUrl: 'payment-type-list.html'
})
export class PaymentTypeListPage {
  plist: PaymentType[];
  oList: Observable<any>;
  noProjectPlist: PaymentType[];  
  vendor: Vendor;
  isNewPaymentType: boolean = false;
  form: FormGroup;
  isReadyToSave: boolean = false;
  dataLoadDone: boolean = false;
  purchase: Purchase;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public storage: Storage, public popoverCtrl: PopoverController, public toastCtrl: ToastController,
    public paymentTypes: PaymentTypes, formBuilder: FormBuilder, navParams: NavParams, public alertCtrl: AlertController) {

    this.plist = [];
    this.noProjectPlist = [];
    this.purchase = navParams.get('purchase');
    /*
  this.oList = this.paymentTypes.query();

  this.oList.subscribe(res => {
      // If the API returned a successful response, capture all payment types

      if (res.status.toLowerCase() == 'success') {
        if(res['body'].length>0){
          for(let _p in res['body']){
            this.plist.push( new PaymentType(res['body'][_p]));
          }
        }
      } else {
      }
    }, err => {
      let toast = this.toastCtrl.create({
        message: "Unable to connect to network",
        duration: 3000,
        position: 'top'
      });
      toast.present();
  }, ()=> console.log('dne'));
  */
    //    this.plist = this.paymentTypes.query();
    this.purchase = navParams.get('purchase');
    this.vendor = navParams.get('vendor');    

    // let payment_types = ;
    let company = this.vendor['company'];
    
    storage.get('companyId').then((val) => {
      console.log("companyID: " + val);
      if (val != 0 && val) {      
        storage.get('employeeId').then((val) => {
          let employeeId = val;
          console.log("company " + company.id);
  
          this.plist = [];
          this.oList = this.paymentTypes.query(val, company.id);
          this.oList.subscribe(res => {
            // If the API returned a successful response, retrive all projects returned
            // if (res.success) {
            //   let payment_types = res['purchases'];
            //   if (payment_types.length > 0) {
            //     for (let _p in payment_types) {
            //       this.plist.push(new PaymentType({
            //         "id": payment_types[_p].id,
            //         "name": payment_types[_p].name
            //       }));
            //     }
            //   }
            // } else {
            //   const alert = this.alertCtrl.create({
            //     title: 'Server Error',
            //     message: 'An error occured',
            //     buttons: ['Dismiss']
            //   });
            //   alert.present();
            // }
  
            console.log("printing paymentTypes");
            console.log(res);
  
            if (res.length > 0) {
              for (let pt in res) {
                let paymentType = res[pt];
                if (paymentType.enabled) {
                  this.plist.push(new PaymentType({
                    "id": paymentType.id,
                    "name": paymentType.name
                  }));
                }
              }
            }
  
            this.noProjectPlist.push(new PaymentType({
              "id": 0,
              "name": "Payment type not found"
            }));
            this.dataLoadDone = true;
          }, err => {
            let toast = this.toastCtrl.create({
              message: "Unable to fetch the payment types.",
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }, () => console.log('done'));
  
        });
      } else {
        this.isNewPaymentType = true;
      }
    });

    // if (this.project.company) {
    //   let payment_types = this.project.company.payment_types;
    //   for (let _p in payment_types) {
    //     this.plist.push(new PaymentType({
    //       "id": payment_types[_p].id,
    //       "name": payment_types[_p].name
    //     }));
    //   }
    //
      // this.noProjectPlist.push(new PaymentType({
      //   "id": 0,
      //   "name": "Payment type not found"
      // }));
    // } else {
    //   this.isNewPaymentType = true;
    // }

    this.form = formBuilder.group({
      name: ['', Validators.required]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
    // this.showtip();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    //    this.viewCtrl.setBackButtonText('Projects');
  }

  showtip() {
    this.storage.get('loadpaymenttypes').then((val) => {
      if (val == null || val) {
        let popover = this.popoverCtrl.create(Tooltips, { page: 'payment-type-list', top: 100 }, { showBackdrop: true });
        popover.present();
      }
      this.storage.set('loadpaymenttypes', false);
    });
  }

  home() {
    this.navCtrl.setRoot(MainPage);
  }

  /**
   * Select payment type
   */
  select(paymentType) {
    if (paymentType.id == 0) {
      this.isNewPaymentType = true;
    } else {
      // this.navCtrl.push(CostCodeListPage, { project: this.project, paymentType: paymentType, purchase: this.purchase });
      this.navCtrl.push(ProjectListPage, { paymentType: paymentType, vendor: this.vendor, purchase: this.purchase });
    }
    // this.navCtrl.push(CostCodeListPage, { project: this.project, paymentType: paymentType, purchase: this.purchase });
    console.log(paymentType);
  }

  newPaymentType() {
    var np: PaymentType = new PaymentType({
      "id": 1,
      "name": this.form.controls['name'].value
    });
    this.select(np);
  }

  // showImage() {
  //   console.log('Show Image :)');
  //   let popover = this.popoverCtrl.create(ImagePopoverPage, { purchase: this.purchase }, { cssClass: 'image-popover' });
  //   popover.present();
  // }

}
