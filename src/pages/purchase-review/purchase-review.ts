import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, Platform } from 'ionic-angular';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';

import { User } from '../../providers/user';
import { Purchases } from '../../providers/providers';
import { Purchase } from '../../models/purchase';
import { Vendor } from '../../models/vendor';
import { ProjectListPage } from '../project-list/project-list';
import { Api } from '../../providers/api';

import { Camera } from '@ionic-native/camera';
import { MainPage } from '../../pages/pages';
import { CostCodeListPage } from '../cost-code-list/cost-code-list';

import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-purchase-review',
  templateUrl: 'purchase-review.html'
})
export class PurchaseReviewPage {
  project: any;
  purchase: Purchase;
  vendor: Vendor;
  paymentType: any;
  costs: any;
  salestax = 0;
  submitTries = 0;
  submitLimit = 5;
  totalAmount = 0;
  totalPurchaseAmount = 0;
  userEmail: string;
  employeeId: any;
  userId: any;
  accountantEmail: string;
  name: string;
  url: string = this.api.url + '/api/purchases?XDEBUG_SESSION_START=PHPSTORM';
  budgetcheckurl: string = this.api.url + '/api/purchases/budgetcheck';
  // url: string = this.api.url + '/api/purchases';
  fileTransfer: TransferObject = this.transfer.create();
  viewOnly: boolean = false;
  dop: string;

  constructor(public loadingCtrl: LoadingController, public user: User, private transfer: Transfer, private file: File, public plt: Platform, private camera: Camera,
    public viewCtrl: ViewController, public api: Api, public navCtrl: NavController, public alertCtrl: AlertController,navParams: NavParams, purchases: Purchases, public storage: Storage,) {

    this.purchase = navParams.get('purchase');
    this.project = navParams.get('project');
    this.vendor = navParams.get('vendor');
    this.paymentType = navParams.get('paymentType');
    this.costs = navParams.get('costs');
    this.salestax = navParams.get('stax');
    this.totalPurchaseAmount = navParams.get('totalpa');
    this.dop = navParams.get('dop');
    this.totalAmount = this.purchase['amount'];    
    // this.userEmail = user._user;
    // console.log(this.vendor);
    console.log(this.costs);
    storage.get('email').then((val) => {
      this.userEmail = val;
    });

    this.storage.get('local_accountantEmail').then((aval) => {
      this.accountantEmail = aval;
    });
    // this.employeeId = user._employeeId;
    storage.get('employeeId').then((val) => {
      if (val) {
        this.employeeId = val;
        // console.log(this.employeeId);
      } else {
        storage.get('userId').then((val) => {
          this.userId = val;
        });
      }
    });
    // this.userId = user._id;
    this.viewOnly = navParams.get('viewOnly');
    // if (this.purchase['purchase_id'] != "") {
    //   purchases.getPurchaseImage(this.purchase['purchase_id']).subscribe(res => {
    //     // If the API returned a successful response, mark the user as logged in
    //
    //     if (res.status.toLowerCase() == 'success') {
    //       //this.purchase['imageData'] = 'data:image/jpeg;base64,' + res['img'];
    //     }
    //   }, err => {
    //     console.log('getPurchaseImage');
    //   }, () => console.log('getPurchaseImage dne'));
    // }
  }

  ionViewDidLoad() {
    //    this.viewCtrl.setBackButtonText('Add Amounts');
  }

  /**
   * Go back to amounts page and allow editing of amounts
   */
  edit() {
    this.navCtrl.push(ProjectListPage, { fromEdit: true });
  }

  /**
   * Submit the purchase details
   */

  submit() {
    this.storage.get('local_name').then((val) => {
      // check if local
      if (val) {
        this.name = val;

        let alert = this.alertCtrl.create({
          title: 'Submit',
          message: 'Type the email of your accountant',
          inputs : [
            {
              name: 'email',
              placeholder: "Enter your accountant's email",
              type: 'email',
              value: this.accountantEmail
            }
          ],
          buttons: [
            {
              text: 'Submit',
              handler: data => {
                let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                let email = data.email;

                if (!re.test(email)) {
                  let iealert = this.alertCtrl.create({
                    title: 'Invalid email',
                    message: "Make sure that you've entered a valid one.",
                    buttons: [
                      {
                        text: 'Ok'
                      }
                    ]
                    // enableBackdropDismiss: false
                  });
                  iealert.present();

                  return;
                }

                if (email.trim().length === 0) {
                  // console.log('email cannot be empty');
                } else {
                  this.storage.set('local_accountantEmail', email);
                  this.accountantEmail = email;
                  this.submitPurchase();
                }
              }
            }
          ]
          // enableBackdropDismiss: false
        });
        alert.present();
      } else {
        this.checkBudgetBeforeSubmit();
        //this.submitPurchase();
      }
    });

  }

  checkBudgetBeforeSubmit() {
    let loader = this.loadingCtrl.create({
      content: 'Submitting purchase. Please wait...'
    });
    loader.present();

    this.purchase['project'] = this.project;
    this.purchase['vendor'] = this.vendor;
    this.purchase['paymentType'] = this.paymentType;
    this.purchase['costs'] = this.costs;
    this.purchase['salestax'] = this.salestax;
    console.log('purchase items:');
    console.log(this.costs);
    // // console.log("desc: " + this.costs[0].cost.description);        
    console.log(this.purchase['costs']);

    let sendParams = {
      projectId: this.project.id,
      projectName: this.project.name,
      employeeId: this.employeeId,
      paymentTypeId: this.paymentType.id,
      paymentTypeName: this.paymentType.name,
      vendorId: this.vendor['id'],
      purchaseItems: JSON.stringify(this.costs),
      purchaseDescription: this.costs[0].cost.description,
      userId: this.userId,
      salesTax: this.salestax,
      dop: this.dop,
      totalAmount: this.totalPurchaseAmount,
      accountantEmail: this.accountantEmail,
      name: this.name
    };

    var purch_data: string = JSON.stringify(this.purchase);

    var cdate: Date = new Date();
    var fdate = cdate.getFullYear() + '-' + cdate.getMonth() + '-' + cdate.getDate() + ' ' + cdate.getHours() + ':' + cdate.getMinutes() + ':' + cdate.getSeconds();
    let params = {
      email: this.userEmail,
      stamp: fdate,
      purch_data: purch_data
    };

    let options: FileUploadOptions = {
      chunkedMode: false,
      fileKey: "purchaseImage",
      fileName: "purchaseimage.jpg",
      mimeType: "image/jpeg",
      params: sendParams
    }

    this.fileTransfer.upload(this.purchase.imageData, encodeURI(this.budgetcheckurl), options)
      .then((data) => {

        var resp = JSON.parse(data.response);
        var costItemsString = "";

        if (resp.isOverBudget == true) {
          resp.listItemsOverBudget.forEach(element => {
            costItemsString += "<li>" + element + "</li>";
          });

          const alert = this.alertCtrl.create({
            title: 'Warning',
            message: `
              <p> Submitting this purchase will cause the following items to exceed the budget.</p>
              <p> </p>
              <ul>
              `+costItemsString+`
              </ul>
              <p>Continuing will notify the approver of this project.</p>
            `,
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                  loader.dismissAll();
                  //this.navCtrl.push(CostCodeListPage, { project: this.project, paymentType: this.paymentType, purchase: this.purchase, vendor: this.vendor });
                  this.home();
                  }
              },
              {
                text: 'Continue',
                handler: () => {
                  loader.dismissAll();
                  this.submitPurchase();
                  }
              }
            ]
          });
          alert.present();
        } else {
          loader.dismissAll();
          this.submitPurchase();
        }
      }, (err) => {
        const alert = this.alertCtrl.create({
          title: 'Server Error',
          message: 'An error occured. error: ' + JSON.stringify(err.body),
          buttons: ['Dismiss']
        });
        alert.present();
        loader.dismissAll();
      });
  }

  submitPurchase() {
    let loader = this.loadingCtrl.create({
      content: 'Submitting purchase. Please wait...'
    });
    loader.present();

    this.purchase['project'] = this.project;
    this.purchase['vendor'] = this.vendor;
    this.purchase['paymentType'] = this.paymentType;
    this.purchase['costs'] = this.costs;
    this.purchase['salestax'] = this.salestax;
    console.log('purchase items:');
    console.log(this.costs);
    // // console.log("desc: " + this.costs[0].cost.description);        
    console.log(this.purchase['costs']);

    let sendParams = {
      projectId: this.project.id,
      projectName: this.project.name,
      employeeId: this.employeeId,
      paymentTypeId: this.paymentType.id,
      paymentTypeName: this.paymentType.name,
      vendorId: this.vendor['id'],
      purchaseItems: JSON.stringify(this.costs),
      purchaseDescription: this.costs[0].cost.description,
      userId: this.userId,
      salesTax: this.salestax,
      dop: this.dop,
      totalAmount: this.totalPurchaseAmount,
      accountantEmail: this.accountantEmail,
      name: this.name
    };

    var purch_data: string = JSON.stringify(this.purchase);

    var cdate: Date = new Date();
    var fdate = cdate.getFullYear() + '-' + cdate.getMonth() + '-' + cdate.getDate() + ' ' + cdate.getHours() + ':' + cdate.getMinutes() + ':' + cdate.getSeconds();
    let params = {
      email: this.userEmail,
      stamp: fdate,
      purch_data: purch_data
    };

    let options: FileUploadOptions = {    
      fileKey: "purchaseImage",
      fileName: "purchaseimage.jpg",
      params: sendParams
    }

    this.fileTransfer.upload(this.purchase.imageData, encodeURI(this.url), options)
      .then((data) => {

        var resp = JSON.parse(data.response);
        if (resp.status.toLowerCase() == 'success') {
          const alert = this.alertCtrl.create({
            title: 'Purchase submitted!',
            message: resp.message,
            buttons: [
              {
                text: 'Ok',
                handler: () => {
                  this.home();
                  }
              }
            ]
          });
          alert.present();
        } else {
          // Auto Retry
          if(this.submitTries >= this.submitLimit){
            this.submitTries++;
            this.submitPurchase();
            loader.dismissAll();
            return;
          }
          const alert = this.alertCtrl.create({
            title: 'Upload Error',
            message: 'Oops your image could not be uploaded! Please try again.',
            buttons: ['Cancel',{text:'Retry',handler:()=>{this.submitPurchase()}},{text:'Retake',handler:()=>{this.retake()}}]
          });
          console.log('Error:');
          console.log(resp.body);
          alert.present();
        }
        loader.dismissAll();
      }, (err) => {
        // Auto Retry
        if(this.submitTries >= this.submitLimit){
          this.submitTries++;
          this.submitPurchase();
          loader.dismissAll();
          return;
        }
        const alert = this.alertCtrl.create({
          title: 'Upload Error',
          message: 'Oops your image could not be uploaded! Please try again.',
          buttons: ['Cancel',{text:'Retry',handler:()=>{this.submitPurchase()}},{text:'Retake',handler:()=>{this.retake()}}]
        });
        console.log('Error:');
        console.log(err.body);
        alert.present();
        loader.dismissAll();
      });
  }

  /**
   * Go back to the image capture on the project list page
   */
  retake() {
    this.getPicture();
  }

  retrySubmit(){
    if(this.submitTries >= this.submitLimit){
      this.submitTries++;
      this.submitPurchase();
    }
  }

  home() {
    this.navCtrl.setRoot(MainPage);
  }

  analyze() {
    // let loader = this.loadingCtrl.create({
    //   content: 'I\'m trying to read your receipt. Please wait...'
    // });
    // loader.present();
    // (<any>window).OCRAD(document.getElementById('image'), text => {
    //   loader.dismissAll();
    //   console.log(text);
    //   alert(text);
    // });
  }

  getPicture() {
    if (Camera['installed']()) {
      var dest: any;
      if (this.plt.is('android')) {
        dest = this.camera.DestinationType.FILE_URI
      } else if (this.plt.is('ios')) {
        dest = this.camera.DestinationType.NATIVE_URI
      } else {
        dest = this.camera.DestinationType.DATA_URL
      }
      this.camera.getPicture({
        // destinationType: dest,
        destinationType: this.camera.DestinationType.FILE_URI,
        quality: 40,
        cameraDirection: 0, //back camera
        // allowEdit: true,
        saveToPhotoAlbum: false,
        correctOrientation: true
      }).then((imageData) => {
        //this.purchase.imageData = 'data:image/jpg;base64,' + imageData;
        this.purchase.imageData = imageData;
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      alert('Camera access is needed.');
    }
  }
}
