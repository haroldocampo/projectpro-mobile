import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Purchase } from '../../models/purchase';

@Component({
  template: `
    <div style="width: 100% !important">
      <img id="image" [src]="purchase.imageData" (click)="close()" style="display: block !important; max-width: 100% !important; max-height: 100% !important" />
    </div>
  `
})
export class ImagePopoverPage {
  purchase: Purchase;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.purchase = this.navParams.get('purchase');
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
